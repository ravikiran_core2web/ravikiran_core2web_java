

class Pr10{

	public static void main(String[]args){
	
		int budget = 5000000;
		switch(budget){
		
			case 5000000:
				System.out.println("H2R");
				break;
			case 1400000:
				System.out.println("ZX10R");
				break;
			case 400000:
				System.out.println("GT650");
				break;
			case 275000:
				System.out.println("Classic RE");
				break;
			case 130000:
				System.out.println("Ola S1 Pro");
				break;
			case 70000:
				System.out.println("Activa");
				break;
			default:
				System.out.println("Try with another budget");

		}
	}
}
