
//Bitwise negation --> toggles each bit 


class BitNegation{
	public static void main(String[]args){
		int x = 5;			//0000 0101
		System.out.println(~x);		//1111 1010 (-6)
		int a = -5;
		System.out.println(~a);
	}
}
