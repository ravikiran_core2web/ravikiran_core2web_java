

class Bitwise20{
	public static void main(String[]args){
		int a = 35;			//0010 0011
		int b = 21;			//0001 0101
		System.out.println(a&b);	//0000 0001 (1)
		System.out.println(a|b);	//0011 0111 (55)
	}
}
