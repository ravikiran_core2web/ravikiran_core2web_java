
//ARITHMETIC OPERATORS = +,-,*,/,%
//all are binary operators (which has two operands)
class ArithmeticOp{
	public static void main(String[]args){
		int x = 100;
		int y = 5;
		System.out.println("Addition= "+(x+y));				//105
		System.out.println("Subtraction= " +(x-y));			//95
		System.out.println("Multiplication= "+(x*y));			//500
		System.out.println("Division= "+(x/y));				//20
		System.out.println("Modulus(for printing remainder) = "+(x%y));	//0
	}
}
